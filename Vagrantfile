# -*- mode: ruby -*-
# vi: set ft=ruby :

CLUSTER_NETWORK_PREFIX = "192.168.43"
CLOUD_LATENCY = "50ms"	# NOTE: Latency is set on all hosts, it goes bi-directional
NUMBER_OF_HOSTS = 3
OSDS_PER_HOST = 3

TEMP_KEY_DIR = "/vagrant/.keys"
DOCKER_IMAGE_DIR = "/vagrant/images"

Vagrant.configure("2") do |config|
  config.vm.box = "bento/ubuntu-20.04"
  config.vm.provider "virtualbox" do |vb|
    vb.cpus = 2
    vb.memory = 2048
  end
  # OSD devices
  # NOTE: 5GB is the minimum size for Ceph to consider a disk "available"
  (0..OSDS_PER_HOST-1).each do |i|
    config.vm.disk :disk, size: "5GB", name: "osd#{i}"
  end

  config.vm.provision "shell", inline: <<~SHELL
    echo 'Installing software ...'
    sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
    apt update
    apt install -y cephadm chrony
    # NOTE: On an HDD server this will actually be slower ...
    #echo 'Loading downloaded Ceph images ...'
    #for img in `ls #{DOCKER_IMAGE_DIR}`; do
    #  docker load -i #{DOCKER_IMAGE_DIR}/$img
    #done
    echo 'Downloading latest Ceph images ...'
    cephadm pull
    echo 'Setting hosts ...'
    for i in {1..#{NUMBER_OF_HOSTS}}; do
      echo "#{CLUSTER_NETWORK_PREFIX}.$i kart-$i" >> /etc/hosts
    done
    echo 'Setting simulated network delay ...'
    tc qdisc add dev eth1 root netem delay #{CLOUD_LATENCY}
  SHELL

  (1..NUMBER_OF_HOSTS).each do |i|
    config.vm.define "kart-#{i}", primary: i==1 do |node|
      IS_BOOTSTRAP = i == 1
      THIS_IP = "#{CLUSTER_NETWORK_PREFIX}.#{i}"
      node.vm.hostname = "kart-#{i}"
      # Data plane network
      node.vm.network "private_network", ip: THIS_IP
      # Expose Ceph Dashboard on bootstrap node
      if IS_BOOTSTRAP then
        node.vm.network "forwarded_port", guest: 8443, host: 8443
      end

      if IS_BOOTSTRAP then
        node.vm.provision "shell", inline: <<~SHELL
          echo 'Bootstrapping Ceph cluster ...'
          cephadm bootstrap --mon-ip #{THIS_IP}
          echo 'Sharing Ceph public key => #{TEMP_KEY_DIR}/ceph.pub'
          mkdir -p /vagrant/.keys
          cp /etc/ceph/ceph.pub #{TEMP_KEY_DIR}/
          echo 'Configuring Ceph to automatically consume available devices ...'
          cephadm shell -- ceph orch apply osd --all-available-devices
        SHELL
      else
        node.vm.provision "shell", inline: <<~SHELL
          echo 'Injecting Ceph public key ...'
          mkdir -p /root/.ssh
          cat #{TEMP_KEY_DIR}/ceph.pub >> /root/.ssh/authorized_keys
        SHELL
      end

    end
  end

end

