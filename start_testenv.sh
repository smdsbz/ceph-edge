#!/usr/bin/bash

echo 'Starting virtual machines ...'
VAGRANT_EXPERIMENTAL='disks' vagrant up

echo 'Testing inter-host latency ...'
for from in kart-{1..3}; do
  echo "==== Initiating ping from $from ===="
  vagrant ssh $from <<-'EOF'
  	for to in kart-{1..3}; do
  	  ping -c 5 $to
  	done
	EOF
done

echo "Adding kart-{2..3} to Ceph cluster ..."
vagrant ssh <<-'EOF'
for node in kart-{2..3}; do
  sudo cephadm shell -- ceph orch host add $node
done
EOF

echo 'You now may login to one of the machines and use "ceph -s" to monitor the Ceph cluster, til all 9 OSDs show up. This may take a while.'

